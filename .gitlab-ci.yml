image: node:20

variables:
  NODE_ENV: test
  DS_EXCLUDED_ANALYZERS: gemnasium-python

default:
  interruptible: true

cache: &cache
  key:
    files:
      - yarn.lock
  paths:
    - node_modules/
  policy: pull

stages:
  - deps
  - test
  - deploy
  - release

deps:
  stage: deps
  script: yarn install --ignore-scripts
  only:
    changes:
      - yarn.lock
  cache:
    <<: *cache
    policy: push

lint:
  stage: test
  script: yarn lint
  only:
    changes:
      - "**/*.js"
      - "**/*.jsx"
      - "**/*.cjs"
      - "**/*.mjs"
      - "**/*.ts"
      - "**/*.tsx"
      - "**/*.scss"
      - "**/*.css"
      - ".eslintignore"
      - ".eslintrc.cjs"
      - ".stylelintrc.json"

build:
  stage: test
  before_script:
    - apt-get update -y && apt-get install -y zip
  script:
    - yarn build
    - cp dist/index.html dist/404.html
    - cd dist && zip -r ../soapbox.zip . && cd ..
  variables:
    NODE_ENV: production
  artifacts:
    paths:
     - soapbox.zip

docs-deploy:
  stage: deploy
  image: alpine:latest
  before_script:
    - apk add curl
  script:
    - curl -X POST -F"token=$CI_JOB_TOKEN" -F'ref=master' https://gitlab.com/api/v4/projects/15685485/trigger/pipeline
  only:
    variables:
      - $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
    changes:
      - "docs/**/*"

review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.git.soapbox.pub
  before_script:
    - apt-get update -y && apt-get install -y unzip
  script:
    - unzip soapbox.zip -d dist
    - npx -y surge dist $CI_COMMIT_REF_SLUG.git.soapbox.pub
  allow_failure: true

pages:
  stage: deploy
  before_script:
    - apt-get update -y && apt-get install -y unzip
  script:
    # artifacts are kept between jobs
    - unzip soapbox.zip -d public
  variables:
    NODE_ENV: production
  artifacts:
    paths:
    - public
  only:
    variables:
      - $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

docker:
  stage: deploy
  image: docker:24.0.6
  services:
    - docker:24.0.6-dind
  tags:
    - dind
  # https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
  interruptible: false

release:
  stage: release
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npx ts-node ./scripts/do-release.ts
  interruptible: false

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
